   /*============================================================================*\  
   ||                      -= NiftyCube Drupal module =-                         ||
   ||                                                                            ||
   || By Jordan Starcher                                                         ||
   || README.txt                                                                 ||
   || Project Version 5.x-1.0                                                    ||
   || June 25, 2007                                                              ||
   || Released under the GNU license                                             ||
   ||                                                                            ||
   ||                              Copyright ©2007                               ||
   ||                       http://www.TheOverclocked.com                        ||
   \*============================================================================*/

Due to licensing limitations, the JavaScript and CSS files needed for NiftyCube cannot be packaged with this module.
Please follow the instructions below to install and complete the module.

1. Download NiftyCube from http://drupal.org/NiftyCube
   and extract the contents into your modules folder.

2. Download NiftyCube from http://www.html.it/articoli/niftycube/NiftyCube.zip

3. Extract the NiftyCube zip folder and place the two .js and .css files
   inside your NiftyCube module folder.

3. Check administer > settings > NiftyCube to see if the files are correctly installed.

Usage instructions are available at http://www.html.it/articoli/niftycube/

Please support by donating. http://www.TheOverclocked.com/donate
